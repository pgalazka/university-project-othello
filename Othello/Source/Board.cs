﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;

namespace Othello
{
    class Board
    {
        #region Variable
       public const int SIZE_X = 8;
       public const int SIZE_Y = 8;

        public enum GameType
        {
            AiVsAi ,
            HumanVsAi,
            HumanVsHuman
        };

        public GameType _gameType = GameType.AiVsAi;

        private bool isDraw = false;
        private bool isGraphic = false;

        public bool _isBlackPlayerTurn = false;

        public Player playerBlack;
        public Player playerWhite;

        public int[,] _board = new int[8, 8]; // 0 -> Empty 1 -> Black 2 -> White
        public int _isBlack = 1;

        private List<Vector2> _allPossibleMoves = new List<Vector2>();

        private List<int> _branch = new List<int>();
        private Vector2 avgBranch_depth = new Vector2(0,1);
        #endregion
        #region Functions
        public Board()
        {
            ClearBoard();
        }
        public void ClearBoard()
        {
            for (int i = 0; i < SIZE_X; i++)
                for (int j = 0; j < SIZE_Y; j++)
                {
                    _board[i, j] = 0;
                }

            _board[3, 3] = 2;
            _board[3, 4] = 1;
            _board[4, 3] = 1;
            _board[4, 4] = 2;
        }
        public void RestartGame()
        {
            ClearBoard();
            GameConfiguration(_gameType, playerBlack, playerWhite, false, 0, isGraphic, isDraw);
        }
        /// <summary>
        /// Configuration of game
        /// </summary>
        /// <param name="gameType">Who is playing, AIvsAI or HUMANvsHUMAN or HUMANvsAI</param>
        /// <param name="Black">Type of AI (only when AIvsAI)</param>
        /// <param name="White">Type of AI</param>
        /// <param name="randomBoard">False for normal board | True for random board</param>
        /// <param name="whoStart">Who should start | 0 for Black | 1 for White | 2 for Random</param>
        /// <param name="isGrap">True is using graphic Version</param>
        /// <param name="printBoard">True if should print boards (only with console version)</param>
        public void GameConfiguration(GameType gameType = GameType.AiVsAi, Player Black = null, Player White = null, bool randomBoard = false, int whoStart = 3, bool isGrap = false, bool printBoard = false)
        {
            //SET PLAYERS
            _gameType = gameType;
            playerBlack = Black;
            playerBlack.SetBoard(this, 1);
            playerWhite = White;
            playerWhite.SetBoard(this, 2);

            isGraphic = isGrap;

            //SET DRAWING BOARD
            isDraw = printBoard;
            if (isDraw) PrintBoard();

            //RANDOM BOARD
            if (randomBoard)
            {
                GenerateRandomBoard();
            }
            else
            {
                ClearBoard();
            }

            //WHO START GAME
            switch (whoStart)
            {
                case 0:
                    {
                        _isBlackPlayerTurn = true;
                        _isBlack = 1;
                        
                    }
                    break;
                case 1:
                    {
                        _isBlackPlayerTurn = false;
                        _isBlack = 2;
                    }
                    break;
                case 2:
                    {
                        _isBlackPlayerTurn = new Random().Next(0, 2) > 0;
                        _isBlack = (_isBlackPlayerTurn) ? 1 : 2;
                    }
                    break;
                default:
                    {
                        _isBlackPlayerTurn = true;
                        _isBlack = 1;
                    }
                        break;
            }


        }
        public bool CanMove(int[,] board ,int i, int j, int player)
        {
            if (board[i,j] != 0) return false;

            int mi, mj, c;
            int oplayer = ((player == 1) ? 2 : 1);

            //UP
            mi = i - 1;
            mj = j;
            c = 0;
            while (mi > 0 && board[mi,mj] == oplayer)
            {
                mi--;
                c++;
            }
            if (mi >= 0 && board[mi, mj] == player && c > 0) return true;


            //DOWN
            mi = i + 1;
            mj = j;
            c = 0;
            while (mi < 7 && board[mi, mj] == oplayer)
            {
                mi++;
                c++;
            }
            if (mi <= 7 && board[mi, mj] == player && c > 0) return true;

            //LEFT
            mi = i;
            mj = j - 1;
            c = 0;
            while (mj > 0 && board[mi, mj] == oplayer)
            {
                mj--;
                c++;
            }
            if (mj >= 0 && board[mi, mj] == player && c > 0) return true;

            //RIGHT
            mi = i;
            mj = j + 1;
            c = 0;
            while (mj < 7 && board[mi, mj] == oplayer)
            {
                mj++;
                c++;
            }
            if (mj <= 7 && board[mi, mj] == player && c > 0) return true;

            //UPLEFT
            mi = i - 1;
            mj = j - 1;
            c = 0;
            while (mi > 0 && mj > 0 && board[mi, mj] == oplayer)
            {
                mi--;
                mj--;
                c++;
            }
            if (mi >= 0 && mj >= 0 && board[mi, mj] == player && c > 0) return true;

            //UPRIGHT
            mi = i - 1;
            mj = j + 1;
            c = 0;
            while (mi > 0 && mj < 7 && board[mi, mj] == oplayer)
            {
                mi--;
                mj++;
                c++;
            }
            if (mi >= 0 && mj <= 7 && board[mi, mj] == player && c > 0) return true;

            //DOWNLEFT
            mi = i + 1;
            mj = j - 1;
            c = 0;
            while (mi < 7 && mj > 0 && board[mi, mj] == oplayer)
            {
                mi++;
                mj--;
                c++;
            }
            if (mi <= 7 && mj >= 0 && board[mi, mj] == player && c > 0) return true;

            //DOWNRIGHT
            mi = i + 1;
            mj = j + 1;
            c = 0;
            while (mi < 7 && mj < 7 && board[mi, mj] == oplayer)
            {
                mi++;
                mj++;
                c++;
            }
            if (mi <= 7 && mj <= 7 && board[mi, mj] == player && c > 0) return true;

            //NOMOVES
            return false;
        }
        public List<Vector2> FindAllPossibleMoves(int[,] board, int player = 0)
        {
            if(player == 0) player = _isBlack;
            List<Vector2> _TMPallPossibleMoves = new List<Vector2>();
            for (int i = 0; i < SIZE_X; i++)
            {
                for (int j = 0; j < SIZE_Y; j++)
                {
                        if (CanMove(board, i, j, player))
                        {
                            _TMPallPossibleMoves.Add(new Vector2(i, j));
                        }
                }
            }
            return _TMPallPossibleMoves;
        }
        public void UpdateBoard(List<Vector2> updatedBoard)
        {
            foreach (var item in updatedBoard)
            {
                _board[(int)item.X, (int)item.Y] = _isBlack;
            }
        }
        public void ChangePlayer()
        {
            _isBlackPlayerTurn = !_isBlackPlayerTurn;
            _isBlack = (_isBlack == 1) ? 2 : 1;
        }
        public List<Vector2> FindAllReversed(int x, int y)
        {
            int score = 0;
            int pX;
            int pY;
            int s;
            List<Vector2> _tmpReversePosition = new List<Vector2>();
            List<Vector2> _tmpPosition = new List<Vector2>();
            

            int player = (_isBlack == 1 ? 1 : 2);
            int opponent = (_isBlack == 1 ? 2 : 1);

            //UP
            pX = x - 1;
            pY = y;
            s = 0;

            while (pX > 0 && _board[pX, pY] == opponent)
            {
                _tmpPosition.Add(new Vector2(pX, pY));
                pX--;
                s++;
            }
            if (pX >= 0 && _board[pX, pY] == player && s > 0)
            {
                _tmpReversePosition.AddRange(_tmpPosition);
                score += s;
            }

            //DOWN
            pX = x + 1;
            pY = y;
            s = 0;
            _tmpPosition.Clear();

            while (pX < 7 && _board[pX, pY] == opponent)
            {
                _tmpPosition.Add(new Vector2(pX, pY));
                pX++;
                s++;
            }
            if (pX <= 7 && _board[pX, pY] == player && s > 0)
            {
                _tmpReversePosition.AddRange(_tmpPosition);
                score += s;
            }

            //LEFT
            pX = x;
            pY = y - 1;
            s = 0;
            _tmpPosition.Clear();

            while (pY > 0 && _board[pX, pY] == opponent)
            {
                _tmpPosition.Add(new Vector2(pX, pY));
                pY--;
                s++;
            }
            if (pY >= 0 && _board[pX, pY] == player && s > 0)
            {
                _tmpReversePosition.AddRange(_tmpPosition);
                score += s;
            }

            //RIGHT
            pX = x;
            pY = y + 1;
            s = 0;
            _tmpPosition.Clear();

            while (pY < 7 && _board[pX, pY] == opponent)
            {
                _tmpPosition.Add(new Vector2(pX, pY));
                pY++;
                s++;
            }
            if (pY <= 7 && _board[pX, pY] == player && s > 0)
            {
                _tmpReversePosition.AddRange(_tmpPosition);
                score += s;
            }

            //UPRIGHT
            pX = x - 1;
            pY = y + 1;
            s = 0;
            _tmpPosition.Clear();

            while (pX > 0 && pY < 7 && _board[pX, pY] == opponent)
            {
                _tmpPosition.Add(new Vector2(pX, pY));
                pY++;
                pX--;
                s++;
            }
            if (pX >= 0 && pY <= 7 && _board[pX, pY] == player && s > 0)
            {
                _tmpReversePosition.AddRange(_tmpPosition);
                score += s;
            }

            //UPLEFT
            pX = x - 1;
            pY = y - 1;
            s = 0;
            _tmpPosition.Clear();

            while (pX > 0 && pY > 0 && _board[pX, pY] == opponent)
            {
                _tmpPosition.Add(new Vector2(pX, pY));
                pY--;
                pX--;
                s++;
            }
            if (pX >= 0 && pY >= 0 && _board[pX, pY] == player && s > 0)
            {
                _tmpReversePosition.AddRange(_tmpPosition);
                score += s;
            }

            //DOWNRIGHT
            pX = x + 1;
            pY = y + 1;
            s = 0;
            _tmpPosition.Clear();

            while (pX < 7 && pY < 7 && _board[pX, pY] == opponent)
            {
                _tmpPosition.Add(new Vector2(pX, pY));
                pY++;
                pX++;
                s++;
            }
            if (pX <= 7 && pY <= 7 && _board[pX, pY] == player && s > 0)
            {
                _tmpReversePosition.AddRange(_tmpPosition);
                score += s;
            }

            //DOWNLEFT
            pX = x + 1;
            pY = y - 1;
            s = 0;
            _tmpPosition.Clear();

            while (pX < 7 && pY > 0 && _board[pX, pY] == opponent)
            {
                _tmpPosition.Add(new Vector2(pX, pY));
                pY--;
                pX++;
                s++;
            }
            if (pX <= 7 && pY >= 0 && _board[pX, pY] == player && s > 0)
            {
                _tmpReversePosition.AddRange(_tmpPosition);
                score += s;
            }

            //RETURN SCORE
            return _tmpReversePosition;
        }
        public bool isGameOver(int[,] board)
        {
            bool canWhite = false;
            bool canBlack = false;

            for (int i = 0; i < SIZE_X; i++)
            {
                for (int j = 0; j < SIZE_Y; j++)
                {
                    if (CanMove(board, i, j, 1))
                    {
                        canBlack = true;
                    }
                    if (CanMove(board, i, j, 2))
                    {
                        canWhite = true;
                    }

                    if(canWhite || canBlack)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public string getWinner(int[,] board)
        {
            int white = 0;
            int black = 0;
            foreach (var item in board)
            {
                if (item == 1) black++;
                else if (item == 2) white++;
            }
            if (black == white) return "Tie";
            return black > white ? "Black" : "White";
        }
        public Vector2 getBranchAndDepth()
        {
            foreach (var item in _branch)
            {
                avgBranch_depth.X += item;
            }
            avgBranch_depth.X = avgBranch_depth.X / _branch.Count();

            return avgBranch_depth;
        }
        public bool GenerateRandomBoard()
        {
            Random rnd = new Random();
            for (int i = 0; i < SIZE_X; i++)
            {
                for (int j = 0; j < SIZE_Y; j++)
                {
                    int random = rnd.Next(0,3);
                    _board[i, j] = random;
                }
            }

           // PrintBoard();

            if (ValidateBoard())
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        bool ValidateBoard()
        {
            int conditionsNumber = 0;
            for (int i = 0; i < SIZE_X; i++)
            {
                for (int j = 0; j < SIZE_Y; j++)
                {
                    if (_board[i, j] != 0)
                    {
                        conditionsNumber = 0;

                        if (i + 1 <= 7)
                        {
                            conditionsNumber++;
                            if (_board[i + 1, j] == 0) conditionsNumber--;
                        }

                         if (i - 1 >= 0)
                        {
                            conditionsNumber++;
                            if (_board[i - 1, j] == 0) conditionsNumber--;
                        }

                        if (j + 1 <= 7)
                        {
                            conditionsNumber++;
                            if (_board[i, j+1] == 0) conditionsNumber--;
                        }

                        if (j - 1 >= 0)
                        {
                            conditionsNumber++;
                            if (_board[i, j - 1] == 0) conditionsNumber--;
                        }

                        if (i + 1 <= 7 && j+1 <=7)
                        {
                            conditionsNumber++;
                            if (_board[i + 1, j+1] == 0) conditionsNumber--;
                        }

                        if (i - 1 >= 7 && j + 1 <= 7)
                        {
                            conditionsNumber++;
                            if (_board[i - 1, j + 1] == 0) conditionsNumber--;
                        }

                        if (i + 1 <= 7 && j - 1 >= 0)
                        {
                            conditionsNumber++;
                            if (_board[i + 1, j - 1] == 0) conditionsNumber--;
                        }

                        if (i - 1 >= 0 && j - 1 >= 0)
                        {
                            conditionsNumber++;
                            if (_board[i - 1, j - 1] == 0) conditionsNumber--;
                        }

                        if (conditionsNumber == 0) 
                        {
                            return false;
                        }
                    }
                }
            }

            

            
            return true;
        }
        public int GetPawn(int i, int j)
        {
            return _board[i, j];
        }
        public int GetAllPlayerPanws(int[,] _board, int player)
        {
            int result = 0;
            foreach (var item in _board)
            {
                if (item == player) result++;
            }
            return result;
        }
        public int GetAllPawns(int[,] _board)
        {
            int result = 0;
            foreach (var item in _board)
            {
                result++;
            }
            return result;
        }
        #region Console game for Complexity
        public void PrintBoard()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            Console.Write("  ");
            for (int i = 0; i < SIZE_X; i++) 
                Console.Write(i + " ");
                Console.WriteLine();

            for (int i=0; i< SIZE_X; i++)
            {
                for (int j = 0; j < SIZE_Y; j++)
                {
                    if(j == 0)
                    Console.Write(i + " ");

                    if (_board[i,j] == 1)
                        Console.Write("○ ");
                    else if (_board[i, j] == 2)
                        Console.Write("● ");
                    else
                        Console.Write("◦ ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
        public void Play()
        {
            if (!isGraphic)
            {
                while (!isGameOver(_board))
                {
                    _allPossibleMoves = FindAllPossibleMoves(_board, _isBlack);
                    if (_allPossibleMoves.Count() > 0)
                    {

                        if (_isBlackPlayerTurn)
                        {
                            if (isDraw) Console.Write("Player Black: \n--------------------------------\n");
                            UpdateBoard(playerBlack.Move());
                        }
                        else
                        {
                            if (isDraw) Console.Write("Player White: \n--------------------------------\n");
                            UpdateBoard(playerWhite.Move());
                        }
                        if (isDraw) PrintBoard();
                    }
                    _branch.Add(_allPossibleMoves.Count());
                    avgBranch_depth.Y++;

                    ChangePlayer();
                }

                if (isDraw) Console.Write($"The winner is: {getWinner(_board)} ");
            }
        }
        #endregion
        #endregion
    }
}
