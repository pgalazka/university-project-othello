﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;


namespace Othello
{
    class Complexity
    {
        public void TreeComplexity(int numbersOfTries, Player player)
        {
            Console.WriteLine($"Tree Complexity");
            Console.WriteLine($"--------------------------------");
            List<Vector2> avgBranch_depth = new List<Vector2>();
            int avgBranch = 0;
            int avgDepth = 0;
            Board b = new Board();
            
            b.GameConfiguration(Board.GameType.AiVsAi, new PCMaxMin(b), new PCMaxMin(b), false, 0, false, false);

            for (int i = 0; i < numbersOfTries; i++)
            {
                b.Play();
                avgBranch_depth.Add(b.getBranchAndDepth());
            }

            foreach (var item in avgBranch_depth)
            {
                avgBranch += (int)item.X;
                avgDepth += (int)item.Y;
            }

            Console.WriteLine($"Branch: {avgBranch / avgBranch_depth.Count()}");
            Console.WriteLine($"Depth: {avgDepth / avgBranch_depth.Count()}");
            Console.WriteLine($"Complexity: {Math.Pow((avgBranch / avgBranch_depth.Count()), avgDepth / avgBranch_depth.Count())}");
        }
        public void RandomCombinationComplexity(int generation, int attempts, Player player)
        {

            Console.WriteLine($"Random Generete Board");
            Console.WriteLine($"--------------------------------");
            int possiblesMove = (int)Math.Pow(64,3);
            Console.WriteLine($"Possible moves 64^3 = {possiblesMove}");

            int[] _complexity = new int[generation];
            double result = 0;

            Board b = new Board();
            b.GameConfiguration(Board.GameType.AiVsAi, new PCGreedyPlayer(b), new PCMaxMin(b), false, 0, true, false);
            for (int i = 0; i < generation; i++)
            {
                _complexity[i] = 0;
                for (int j = 0; j < attempts; j++)
                {
                    
                    if (b.GenerateRandomBoard())
                        _complexity[i] ++;
                }
                Console.WriteLine($"Good attemps at {i} generation: {_complexity[i]}");
            }

            for (int i = 0; i < generation; i++)
            {
                _complexity[i] = (_complexity[i]/attempts) * possiblesMove;
                result += _complexity[i];
            }

            result /= generation;
            Console.WriteLine($"\nResult: {result}");
        }
    }
}
