﻿using System;

namespace Othello
{
    class GameManager
    {
        
        static void Main(string[] args)
        {
            Board b = new Board();
            b.GameConfiguration(Board.GameType.AiVsAi, PlayerDictionary.PlayerList[4], PlayerDictionary.PlayerList[4], false, 0, true, false);
            g_Board gBoard = new g_Board(b);

            /*Complexity _complexity = new Complexity();
            Console.WriteLine($"Exercise 1");
            Console.WriteLine($"--------------------------------");
            _complexity.TreeComplexity(1000, new PCGreedyPlayer());
            Console.WriteLine();
            Console.WriteLine($"Exercise 2");
            Console.WriteLine($"--------------------------------");
            _complexity.RandomCombinationComplexity(10, 1000, new PCGreedyPlayer());*/

            Console.ReadKey();
        }
    }
}
