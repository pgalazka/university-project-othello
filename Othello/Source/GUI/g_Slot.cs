﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Othello
{
    class g_Slot
    {
        Image _lastImage;
        Label _slot = new Label();
        public bool isPawn = false;

        public g_Slot(int x, int y)
        {
            _slot.Name = x+" "+y;
            _slot.Size = new System.Drawing.Size(50, 50);
            _slot.Image = g_LoadResources.i_BackGround;
            _slot.MouseEnter += new EventHandler(onMouseEnter);
            _slot.MouseLeave += new EventHandler(onMouseExit);

        }
        public Label getSlot()
        {
            return _slot;
        }
        public void setPosition(int width, int height)
        {
            _slot.Location = new System.Drawing.Point(width, height);
        }
        public void setPawn(int i)
        {
            switch (i)
            {
                case 0: _slot.Image = g_LoadResources.i_BackGround; break;
                case 1: { _slot.Image = g_LoadResources.i_Black; isPawn = true; } break;
                case 2: { _slot.Image = g_LoadResources.i_White; isPawn = true; } break;
                case 3: { _slot.Image = g_LoadResources.i_AvailableSlot; } break;
                default: _slot.Image = g_LoadResources.i_BackGround; break;
            }
        }
        public void onMouseEnter(object sender, EventArgs e)
        {
            if (!isPawn)
            {
                _lastImage = _slot.Image;
                _slot.Image = g_LoadResources.i_Hover;
            }
        }
        public void onMouseExit(object sender, EventArgs e)
        {
            if (!isPawn)
            {
                _slot.Image = _lastImage;
            }
        }
    }
}
