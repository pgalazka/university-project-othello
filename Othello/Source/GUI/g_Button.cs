﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Othello
{
    class g_Button
    {
        Label _btn = new Label();
        Image _normal;
        Image _hover;
        Image _clicked;
        Image _lastImage;

        public g_Button(int width, int height,int posX, int posY, Image normal, Image hover, Image clicked)
        {
            _btn.Size = new Size(width, height);
            _btn.Location = new Point(posX, posY);
            _btn.Image = normal;
            _normal = normal;
            _hover = hover;
            _clicked = clicked;

            _btn.MouseDown += onMouseDown;
            _btn.MouseUp += onMouseUp;
            _btn.MouseEnter += onMouseEnter;
            _btn.MouseLeave += onMouseExit;
        }
        public Label getButton()
        {
            return _btn;
        }
        public void onMouseEnter(object sender, EventArgs e)
        {
                _btn.Image = _hover;
        }
        public void onMouseExit(object sender, EventArgs e)
        {
                _btn.Image = _normal;
        }
        public void onMouseDown(object sender, EventArgs e)
        {
            _lastImage = _btn.Image;
            _btn.Image = _clicked;
        }
        public void onMouseUp(object sender, EventArgs e)
        {
            _btn.Image = _lastImage;
            
        }
    }
}
