﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Othello
{
    class g_MSGBox
    {
        private Form _msgBox = new Form();
        private Label _message = new Label();
        public Button b_yes = new Button();
        public Button b_no = new Button();

        public g_MSGBox(string message, string formName, string bt_yes, string bt_no)
        {
            _msgBox.Size = new Size(300, 200);
            _msgBox.Text = formName;
            _msgBox.Icon = g_LoadResources.Ico;
            _msgBox.ControlBox = false;
            _msgBox.FormBorderStyle = FormBorderStyle.FixedSingle;
            _msgBox.StartPosition = FormStartPosition.CenterScreen;
            _msgBox.BackColor = Color.FromArgb(26, 188, 156);
            _message.Size = new Size(280, 100);
            _message.Text = message;
            _message.Font = new Font("Open Sans", 16);
            _message.TextAlign = ContentAlignment.MiddleCenter;

            b_yes.Size = new Size(100, 25);
            b_yes.Text = bt_yes;
            b_yes.Font  = new Font("Open Sans", 10);
            b_yes.TextAlign = ContentAlignment.MiddleCenter;
            b_yes.Location = new Point(30, 100);

            b_no.Size = new Size(100, 25);
            b_no.Text = bt_no;
            b_no.Font = new Font("Open Sans", 10);
            b_no.TextAlign = ContentAlignment.MiddleCenter;
            b_no.Location = new Point(140, 100);
            b_yes.Click += HideBox;

            _msgBox.Controls.Add(b_yes);
            _msgBox.Controls.Add(b_no);
            _msgBox.Controls.Add(_message);
            _msgBox.Show();
        }
        void HideBox(object sender, EventArgs e)
        {
            _msgBox.Hide();
        }
    }
}
