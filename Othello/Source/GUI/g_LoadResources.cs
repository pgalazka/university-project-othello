﻿using System.Drawing;

namespace Othello
{
    static class g_LoadResources
    {
        static string i_Path = "..\\..\\Resources\\img\\";

        public static Image i_BackGround = Image.FromFile($"{i_Path}BackGround.png");
        public static Image i_AvailableSlot = Image.FromFile($"{i_Path}Hover.png");
        public static Image i_Hover = Image.FromFile($"{i_Path}Click.png");
        public static Image i_Black = Image.FromFile($"{i_Path}Black.png");
        public static Image i_White = Image.FromFile($"{i_Path}White.png");
        public static Image i_AcctualPlayerWhite = Image.FromFile($"{i_Path}acctualPlayerWhite.png");
        public static Image i_AcctualPlayerBlack = Image.FromFile($"{i_Path}acctualPlayerBlack.png");
        public static Image i_Exit = Image.FromFile($"{i_Path}Exit.png");
        public static Image i_ExitHover = Image.FromFile($"{i_Path}ExitHover.png");
        public static Image i_ExitClick = Image.FromFile($"{i_Path}ExitClick.png");
        public static Image i_Retry = Image.FromFile($"{i_Path}Retry.png");
        public static Image i_RetryHover = Image.FromFile($"{i_Path}RetryHover.png");
        public static Image i_RetryClick = Image.FromFile($"{i_Path}RetryClick.png");
        public static Image i_PNS = Image.FromFile($"{i_Path}PNS.png");
        public static Image i_PNSHover = Image.FromFile($"{i_Path}PNSHover.png");
        public static Image i_PNSClick = Image.FromFile($"{i_Path}PNSClick.png");
        public static Icon Ico = new Icon($"{i_Path}icon.ico");
    }
}
