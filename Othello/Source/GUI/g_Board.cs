﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Windows.Forms;

namespace Othello
{
    class g_Board
    {
        private Board _acctualBoard;
        private Form g_board = new Form();
        private g_Settings g_sett;
        private g_Slot[,] _buttons = new g_Slot[8,8];
        private Label _acctualPlayer = new Label();

        private g_Button g_retry = new g_Button(60,60,430,280,g_LoadResources.i_Retry, g_LoadResources.i_RetryHover, g_LoadResources.i_RetryClick);
        private g_Button g_exit = new g_Button(60,60,430, 350,g_LoadResources.i_Exit, g_LoadResources.i_ExitHover, g_LoadResources.i_ExitClick);
        private g_Button g_PNS = new g_Button(60,60,430, 210,g_LoadResources.i_PNS, g_LoadResources.i_PNSHover, g_LoadResources.i_PNSClick);
        private Label _retry;
        private Label _exit;
        private Label _PNS;

        PNS _pns;

        private bool _isBotStop = false;

        public g_Board(Board b)
        {
            _acctualBoard = b;
            CreateBoard();
            InitGameView();
        }
        private void InitGameView()
        {
            _pns = new PNS(_acctualBoard);
            g_board.Text = "Othello";
            g_board.Icon = g_LoadResources.Ico;
            g_board.Size = new Size(520, 460);
            g_board.MaximizeBox = false;
            g_board.MinimizeBox = false;
            g_board.FormBorderStyle = FormBorderStyle.FixedSingle;
            g_board.StartPosition = FormStartPosition.CenterScreen;
            g_board.BackColor = Color.FromArgb(26, 188, 156);

            _acctualPlayer.Size = new Size(60,60);
            _acctualPlayer.Location = new Point(g_board.Width-90, 10);
            _acctualPlayer.Image = (_acctualBoard._isBlackPlayerTurn ? g_LoadResources.i_AcctualPlayerBlack : g_LoadResources.i_AcctualPlayerWhite);

            _retry = g_retry.getButton();
            _retry.Click += RestartGame;
            _retry.Click += StopBot;
            _exit = g_exit.getButton();
            _exit.Click += openSettings;
            _PNS = g_PNS.getButton();
            _PNS.Click += PNSShow;

            g_board.Controls.Add(_retry);
            g_board.Controls.Add(_exit);
            g_board.Controls.Add(_PNS);
            g_board.Controls.Add(_acctualPlayer);
            g_board.Shown += ChoseGameType;
            g_board.ShowDialog();
        }
        private void ChoseGameType(object sender = null, EventArgs e = null)
        {
            if (_acctualBoard._gameType == Board.GameType.AiVsAi)
            {
                _isBotStop = false;
                AIvsAI();
            }
            else
            {
                MarkPossibleMove();
            }
        }
        private void CreateBoard()
        {
            int height = 10;
            int width = 10;
            for (int i = 0; i < 8; i++)
            {
                for(int j=0; j<8; j++)
                {
                    _buttons[i, j] = new g_Slot(i,j);
                    _buttons[i, j].setPosition(width, height);
                    _buttons[i, j].setPawn(_acctualBoard.GetPawn(i, j));
                    _buttons[i, j].getSlot().Click += GameType;
                    g_board.Controls.Add(_buttons[i, j].getSlot());
                    width += 50;
                }
                width = 10;
                height += 50;
            }
        }
        private void GameType(object sender, EventArgs e)
        {
            if (_acctualBoard._gameType == Board.GameType.HumanVsHuman)
                HumanVsHuman(sender);
            else if (_acctualBoard._gameType == Board.GameType.HumanVsAi)
                HumanVsAi(sender);
        }
        private void RefreshBoard()
        {
            _acctualPlayer.Image = (_acctualBoard._isBlackPlayerTurn ? g_LoadResources.i_AcctualPlayerBlack : g_LoadResources.i_AcctualPlayerWhite);

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    _buttons[i, j].setPawn(_acctualBoard.GetPawn(i, j));
                }
            }
        }
        private void MarkPossibleMove()
        {
            var possible = _acctualBoard.FindAllPossibleMoves(_acctualBoard._board, _acctualBoard._isBlack);

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (_acctualBoard._board[i, j] == 0)
                        _buttons[i, j].setPawn(0);
                }
            }

            foreach (var item in possible)
            {
                _buttons[(int)item.X, (int)item.Y].setPawn(3);
            }
        }
        private void AIvsAI()
        {
            while (!_acctualBoard.isGameOver(_acctualBoard._board) && !_isBotStop)
            {
                var _allPossibleMoves = _acctualBoard.FindAllPossibleMoves(_acctualBoard._board, _acctualBoard._isBlack);
                if (_allPossibleMoves.Count() > 0)
                {
                    if (_acctualBoard._isBlackPlayerTurn)
                    {
                        _acctualBoard.UpdateBoard(_acctualBoard.playerBlack.Move());
                    }
                    else
                    {
                        _acctualBoard.UpdateBoard(_acctualBoard.playerWhite.Move());
                    }
                }
                _acctualBoard.ChangePlayer();
                RefreshBoard();
                Addons.Wait(100);
            }
            if (!_isBotStop)
                TryAgain();
        }
        private void HumanVsAi(object sender)
        {
            Vector2 position = GetPosition(sender);
            var possible = _acctualBoard.FindAllPossibleMoves(_acctualBoard._board, _acctualBoard._isBlack);

            if (possible.Count > 0)
            { 
                if (possible.Any(p => p.X == position.X && p.Y == position.Y))
                {
                    if (!_buttons[(int)position.X, (int)position.Y].isPawn)
                    {
                        List<Vector2> upBoard = new List<Vector2>();
                        _buttons[(int)position.X, (int)position.Y].isPawn = true;
                        upBoard.AddRange(_acctualBoard.FindAllReversed((int)position.X, (int)position.Y));
                        upBoard.Add(new Vector2(position.X, position.Y));
                        _acctualBoard.UpdateBoard(upBoard);
                        _acctualBoard.ChangePlayer();
                        RefreshBoard(); 

                        Addons.Wait(500);

                        if (_acctualBoard.FindAllPossibleMoves(_acctualBoard._board, _acctualBoard._isBlack).Count > 0)
                            _acctualBoard.UpdateBoard(_acctualBoard.playerWhite.Move());
                        _acctualBoard.ChangePlayer();
                        RefreshBoard();
                        MarkPossibleMove();
                    }
                }
            }
            if(_acctualBoard.isGameOver(_acctualBoard._board))
            {
                TryAgain();
            }
        }
        private void HumanVsHuman(object sender)
        {
            Vector2 position = GetPosition(sender);

            var possible = _acctualBoard.FindAllPossibleMoves(_acctualBoard._board, _acctualBoard._isBlack);

            if (possible.Count > 0)
            {
                if (possible.Any(p => p.X == position.X && p.Y == position.Y))
                {
                    if (!_buttons[(int)position.X, (int)position.Y].isPawn)
                    {
                        List<Vector2> upBoard = new List<Vector2>();
                        _buttons[(int)position.X, (int)position.Y].isPawn = true;
                        upBoard.AddRange(_acctualBoard.FindAllReversed((int)position.X, (int)position.Y));
                        upBoard.Add(new Vector2(position.X, position.Y));
                        _acctualBoard.UpdateBoard(upBoard);
                        _acctualBoard.ChangePlayer();
                        RefreshBoard();
                        MarkPossibleMove();
                    }
                }
            }
            if (_acctualBoard.isGameOver(_acctualBoard._board))
            {
                TryAgain();
            }
        }
        private Vector2 GetPosition(object obj)
        {
            Label clickedButton = (Label)obj;
            var coordinate = clickedButton.Name.Split(' ');
            return new Vector2(int.Parse(coordinate[0]), int.Parse(coordinate[1]));
        }
        private void TryAgain()
        {
            g_MSGBox msgBox = new g_MSGBox($"The winner is: {_acctualBoard.getWinner(_acctualBoard._board)}", "End of Game", "Try again", "Leave");
            msgBox.b_yes.Click += RestartGame;
            msgBox.b_no.Click += QuitGame;
        }
        private void QuitGame(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void openSettings(object sender, EventArgs e)
        {
            g_sett = new g_Settings(_acctualBoard, this);
            StopBot(null, null);
            g_sett.Show();
            g_sett.b_back.Click += StopBot;
        }

        public void RestartGame(object sender, EventArgs e)
        {
            foreach (var item in _buttons)
            {
                item.isPawn = false;
            }
            _acctualBoard.RestartGame();
            RefreshBoard();
            ChoseGameType();
        }
        public void StopBot(object sender, EventArgs e)
        {
            _isBotStop = true;
        }

        public void PNSShow(object sender, EventArgs e)
        {
            Node n = new Node();
            n.board = _acctualBoard._board;
            _pns.pns(n);
        }
    }
}
