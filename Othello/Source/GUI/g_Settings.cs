﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Othello
{
    class g_Settings
    {
        #region GUI
        private Label l_settings = new Label();
        private Form f_settings = new Form();
        private Label l_gameType = new Label();
        private ComboBox ddl_gameType = new ComboBox();
        private Label l_playerOne = new Label();
        private ComboBox ddl_playerOne = new ComboBox();
        private Label l_playerTwo = new Label();
        private ComboBox ddl_playerTwo = new ComboBox();
        public Button b_backAndHide = new Button();
        public Button b_back = new Button();
        #endregion

        private Board _acctualBoard;
        private g_Board _acctualGBoard;

        private int GameType = 0;
        private int PlayerOne = 0;
        private int PlayerTwo = 0;

        public g_Settings(Board _ab, g_Board _agb)
        {
            _acctualBoard = _ab;
            _acctualGBoard = _agb;
            InitLabels();
            InitDropDownList();
            InitButton();
            InitForm();
        }
        private void InitForm()
        {
            f_settings.Text = "Othello - Settings";
            f_settings.Icon = g_LoadResources.Ico;
            f_settings.Size = new Size(300, 500);
            f_settings.MaximizeBox = false;
            f_settings.MinimizeBox = false;
            f_settings.FormBorderStyle = FormBorderStyle.FixedSingle;
            f_settings.StartPosition = FormStartPosition.CenterScreen;
            f_settings.BackColor = Color.FromArgb(26, 188, 156);
            f_settings.Controls.Add(b_back);
            f_settings.Controls.Add(b_backAndHide);
            f_settings.Controls.Add(ddl_gameType);
            f_settings.Controls.Add(ddl_playerOne);
            f_settings.Controls.Add(ddl_playerTwo);
            f_settings.Controls.Add(l_playerOne);
            f_settings.Controls.Add(l_playerTwo);
            f_settings.Controls.Add(l_gameType);
            f_settings.Controls.Add(l_settings);
        }
        private void InitButton()
        {
            b_backAndHide.Size = new Size(150, 25);
            b_backAndHide.Text = "Save and Close";
            b_backAndHide.Font = new Font("Open Sans", 10);
            b_backAndHide.TextAlign = ContentAlignment.MiddleCenter;
            b_backAndHide.Location = new Point(65, 400);
            b_backAndHide.BackColor = Color.White;


            b_back.Size = new Size(100, 25);
            b_back.Text = "Save";
            b_back.Font = new Font("Open Sans", 10);
            b_back.TextAlign = ContentAlignment.MiddleCenter;
            b_back.Location = new Point(90, 365);
            b_back.BackColor = Color.White;

            b_backAndHide.Click += SaveAndHide;
            b_back.Click += Save;
        }
        private void InitDropDownList()
        {
            ddl_gameType.DropDownStyle = ComboBoxStyle.DropDownList;
            ddl_gameType.DataSource = (Enum.GetValues(typeof(Board.GameType)));
            ddl_gameType.SelectedItem = Board.GameType.AiVsAi;
            ddl_gameType.Size = new Size(200, 25);
            ddl_gameType.Location = new Point(45, 140);
            ddl_gameType.Font = new Font("Open Sans", 15);

            ddl_playerOne.DropDownStyle = ComboBoxStyle.DropDownList;
            ddl_playerOne.DataSource = (Enum.GetValues(typeof(AvaliblePlayers)));
            ddl_playerOne.SelectedItem = Board.GameType.AiVsAi;
            ddl_playerOne.Size = new Size(200, 25);
            ddl_playerOne.Location = new Point(45, 215);
            ddl_playerOne.Font = new Font("Open Sans", 15);

            ddl_playerTwo.DropDownStyle = ComboBoxStyle.DropDownList;
            ddl_playerTwo.DataSource = (Enum.GetValues(typeof(AvaliblePlayers)));
            ddl_playerTwo.SelectedItem = Board.GameType.AiVsAi;
            ddl_playerTwo.Size = new Size(200, 25);
            ddl_playerTwo.Location = new Point(45, 290);
            ddl_playerTwo.Font = new Font("Open Sans", 15);

            ddl_gameType.SelectedValueChanged += onValuChange;
            ddl_playerOne.SelectedValueChanged += onValuChange;
            ddl_playerTwo.SelectedValueChanged += onValuChange;

        }
        private void InitLabels()
        {
            l_settings.Text = "Settings";
            l_settings.Size = new Size(240, 80);
            l_settings.Font = new Font("Open Sans", 25);
            l_settings.Location = new Point(30, 0);
            l_settings.TextAlign = ContentAlignment.MiddleCenter;

            l_gameType.Text = "Type of game";
            l_gameType.Size = new Size(220, 40);
            l_gameType.Font = new Font("Open Sans", 15);
            l_gameType.Location = new Point(40, 105);
            l_gameType.TextAlign = ContentAlignment.MiddleLeft;

            l_playerOne.Text = "Player One";
            l_playerOne.Size = new Size(220, 40);
            l_playerOne.Font = new Font("Open Sans", 15);
            l_playerOne.Location = new Point(40, 180);
            l_playerOne.TextAlign = ContentAlignment.MiddleLeft;

            l_playerTwo.Text = "Player Two";
            l_playerTwo.Size = new Size(220, 40);
            l_playerTwo.Font = new Font("Open Sans", 15);
            l_playerTwo.Location = new Point(40, 255);
            l_playerTwo.TextAlign = ContentAlignment.MiddleLeft;
        }
        private void SaveSettings()
        {
            Player playerOne = null;
            Player playerTwo = null;
            Board.GameType gameType = Board.GameType.AiVsAi;

            foreach (AvaliblePlayers item in (AvaliblePlayers[])Enum.GetValues(typeof(AvaliblePlayers)))
            {
                if (PlayerOne == (int)item)
                    playerOne = PlayerDictionary.PlayerList[(int)item];
                if (PlayerTwo == (int)item)
                    playerTwo = PlayerDictionary.PlayerList[(int)item];
            }
            foreach (Board.GameType item in (Board.GameType[])Enum.GetValues(typeof(Board.GameType)))
            {
                if (GameType == (int)item)
                    gameType = item;
            }

            _acctualBoard.GameConfiguration(gameType, playerOne, playerTwo, false, 0, true, false);
            _acctualGBoard.RestartGame(null, null);
        }
        private void onValuChange(object sender, EventArgs e)
        {
            ComboBox res = sender as ComboBox;
            if (sender.Equals(ddl_gameType))
            {
                GameType = (int)res.SelectedValue;  
            }
            else if(sender.Equals(ddl_playerOne))
            {
                PlayerOne = (int)res.SelectedValue;
            }
            else
            {
                PlayerTwo = (int)res.SelectedValue;
            }
        }

        public Button getBackButton()
        {
            return b_back;
        }
        public void Show()
        {
            f_settings.TopMost = true;
            f_settings.Show();
        }
        public void SaveAndHide(object sender, EventArgs e)
        {
            f_settings.Hide();
            SaveSettings();
        }
        public void Save(object sender, EventArgs e)
        {
            SaveSettings();
        }
    }
}
