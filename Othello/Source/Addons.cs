﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Othello
{
    static class Addons
    {
        public static int ReadInt(this string character)
        {
            return Convert.ToInt32(character);
        }
        public static int[] ReadMultiplyInt(this string character)
        {
            var _characters = character.Split(' ');
            int[] result = new int[_characters.Length];
            for (int i = 0; i < _characters.Length; i++)
            {
                result[i] = Convert.ToInt32(_characters[i]);
            }
            return result;
        }
        public static void Wait(int milliseconds)
        {
            var timer1 = new System.Windows.Forms.Timer();
            if (milliseconds == 0 || milliseconds < 0) return;

            timer1.Interval = milliseconds;
            timer1.Enabled = true;
            timer1.Start();

            timer1.Tick += (s, e) =>
            {
                timer1.Enabled = false;
                timer1.Stop();
            };

            while (timer1.Enabled)
            {
                Application.DoEvents();
            }
        }
    }
}
