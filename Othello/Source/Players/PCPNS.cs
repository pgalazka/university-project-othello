﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Othello
{
    class PNS
    {
        Board _board;
        int _player;
        int _oponent;
        int counter;
        public PNS(Board b)
        {
            _board = b;
            _player = b._isBlack;
            _oponent = _player == 1 ? 2 : 1;
        }

        public void pns(Node root)
        {
            counter = 0;
            evaluation(root.board);
            SetProofAndDisproof(root);
            Node current = root;
            Node mostProviding;

            while (root.proof != 0 && root.proof != 0 && counter < 100)
            {
                mostProviding = SelectMostProvidedNode(current);
                expandNode(mostProviding);
                current = updateAncestor(mostProviding, root);
                counter++;
            }

            Console.WriteLine("Proof: " + current.proof);
            Console.WriteLine("DisProof: " + current.disproof);
        }

        public int evaluation(int[,] root)
        {
            int state;
            if (_board.isGameOver(root))
            {
                if (_board.getWinner(root) == "Black")
                {
                    if (_player == 1) state = 1;
                    else state = -1;
                }
                else if (_board.getWinner(root) == "White")
                {
                    if (_player == 2) state = 1;
                    else state = -1;
                }
                else
                    state = 0;
            }
            else
                state = 0;

            return state;
        }
        void SetProofAndDisproof(Node node)
        {
            if(node.expanded)
            {
                if(!node.type) // AND
                {
                    node.proof = 0;
                    node.disproof = int.MaxValue;
                    foreach (Node child in node.children)
                    {
                        node.proof += child.proof;
                        node.disproof = Math.Min(node.disproof, child.disproof);
                    }
                }
                else // OR
                {
                    node.proof = int.MaxValue;
                    node.disproof = 0;
                    foreach (Node child in node.children)
                    {
                        node.disproof += child.disproof;
                        node.proof = Math.Min(node.proof, child.proof);
                    }
                }
            }
            else
            {
                switch(node.value)
                {
                    case -1:
                        {
                            node.proof = int.MaxValue;
                            node.disproof = 0;
                        }
                    break;
                    case 0:
                        {
                            node.proof = 1;
                            node.disproof = 1;
                        }
                    break;
                    case 1:
                        {
                            node.proof = 0;
                            node.disproof = int.MaxValue;
                        }
                    break;
                }
            }
        }
        Node SelectMostProvidedNode(Node node)
        {
            Node best = new Node();

            while(node.expanded)
            {
                int value = int.MaxValue;
                if(node.type)
                {
                    foreach (Node child in node.children)
                    {
                        if (value > child.disproof)
                        {
                            best = child;
                            value = child.disproof;
                        }
                    }
                }
                else
                {
                    foreach (Node child in node.children)
                    {
                        if (value > child.proof)
                        {
                            best = child;
                            value = child.proof;
                        }
                    }
                }
                node = best;
            }
            return node;
        }
        Node expandNode(Node node)
        {
            node.children = generateChildren(node);
            foreach (Node child in node.children)
            {
                evaluation(child.board);
                SetProofAndDisproof(child);
                if(!node.type)
                {
                    if (child.disproof == 0)
                        return node;
                }
                else
                {
                    if (child.proof == 0)
                        return node;
                }    
            }
            node.expanded = true;
            return node;
        }
        List<Node> generateChildren(Node node)
        {
            List<Vector2> possibleMoves = _board.FindAllPossibleMoves(node.board, _player);
            List<Node> childrens = new List<Node>();
            for (int i = 0; i < possibleMoves.Count; i++)
            {
                Node child = new Node();
                child.type = !node.type;
                child.parent = node;

                //Copy Table
                for (int j = 0; j < 8; j++)
                {
                    for (int k = 0; k < 8; k++)
                    {
                        child.board[j, k] = _board._board[j, k];
                    }
                }

                //New Table
                List<Vector2> position = new List<Vector2>();
                position.Add(possibleMoves[i]);
                position.AddRange(_board.FindAllReversed((int)possibleMoves[i].X, (int)possibleMoves[i].Y));
                
                foreach (var item in position)
                {
                    child.board[(int)item.X, (int)item.Y] = (child.type ? _oponent : _player);
                }
                childrens.Add(child);
            }
            return childrens;
        }
        Node updateAncestor(Node node, Node root)
        {
            int oldProof;
            int oldDisProof;

            while (node != root)
            {
                oldProof = node.proof;
                oldDisProof = node.disproof;
                SetProofAndDisproof(node);
                if(node.proof == oldProof && node.disproof == oldDisProof)
                {
                    return node;
                }
                node = node.parent;
            }
            SetProofAndDisproof(root);
            return root;
        }

    }

    class Node
    {
        public Node parent { get; set; } = null;
        public int[,] board { get; set; } = new int[8,8];
        public bool expanded { get; set; } = false;
        public bool type { get; set; } = false; // false => AND true => OR
        public int proof { get; set; } = int.MaxValue;
        public int disproof { get; set; } = int.MaxValue;
        public int value { get; set; } = 0;
        public List<Node> children { get; set; } = new List<Node>();
    }
}
