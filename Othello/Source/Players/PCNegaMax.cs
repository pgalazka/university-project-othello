﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Othello
{
    class PCNegaMax : Player
    {
 
        private Board _acctualBoard;
        private int[,] _tmpBoard = new int[8,8];
        private int _acctualPlayer;
        private int _oponent;
        private Vector2 bestMove;


        public PCNegaMax(Board b = null)
        {
            _acctualBoard = b;
        }
        public override List<Vector2> Move()
        {
            var possibleMoves = _acctualBoard.FindAllPossibleMoves(_acctualBoard._board, _acctualBoard._isBlack);

            int bestScore = int.MinValue;
            int score;

            for (int i = 0; i < possibleMoves.Count; i++)
            {

                getAcctualBoard();
                newBoardAfterMove((int)possibleMoves[i].X, (int)possibleMoves[i].Y, _acctualPlayer);
                score = -negaMax(5, -1);
                if (score > bestScore)
                {
                    bestScore = score;
                    bestMove = possibleMoves[i];
                }
            }

            List<Vector2> moveTo = new List<Vector2>();
            moveTo.AddRange(_acctualBoard.FindAllReversed((int)bestMove.X, (int)bestMove.Y));
            moveTo.Add(bestMove);
            return moveTo;
        }

        public override void SetBoard(Board board, int whoAmI)
        {
            _acctualBoard = board;
            _acctualPlayer = whoAmI;
            _oponent = _acctualPlayer == 1 ? 2 : 1;
        }

        int negaMax(int depth, int side)
        {
            int score;

            if (_acctualBoard.isGameOver(_tmpBoard) || depth == 0)
            {
                return eval() * side;
            }

            if (side == 1 && _acctualBoard.FindAllPossibleMoves(_tmpBoard, _acctualPlayer).Count <= 0 || side == -1 && _acctualBoard.FindAllPossibleMoves(_tmpBoard, _oponent).Count <= 0)
            {
                return -negaMax(depth - 1, -side);
            }

            var possibleMoves = _acctualBoard.FindAllPossibleMoves(_tmpBoard, side==1 ? _acctualPlayer : _oponent);
            score = int.MinValue;
            for (int i = 0; i < possibleMoves.Count; i++)
            {
                newBoardAfterMove((int)possibleMoves[i].X, (int)possibleMoves[i].Y, side == 1 ? _acctualPlayer : _oponent);
                score = Math.Max(score, -negaMax(depth-1, -side));
            }
            return score;
        }

        void getAcctualBoard()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    _tmpBoard[i, j] = _acctualBoard._board[i, j];
                }
            }
        }
        void newBoardAfterMove(int x, int y, int _person)
        {

            List<Vector2> moveTo = new List<Vector2>();
            moveTo.AddRange(_acctualBoard.FindAllReversed(x, y));
            moveTo.Add(new Vector2(x, y));
            foreach (var item in moveTo)
            {
                _tmpBoard[(int)item.X, (int)item.Y] = _person;
            }
        }

        #region CountBestScore
        public int eval()
        {
            int mob = evalMobility();
            int sc = evalDiscDiff();
            return 2 * mob + sc + 1000 * evalCorner();
        }
        public int evalDiscDiff()
        {
            int mySC = _acctualBoard.GetAllPlayerPanws(_tmpBoard, _acctualPlayer);
            int opSC = _acctualBoard.GetAllPlayerPanws(_tmpBoard, _oponent);
            return 100 * (mySC - opSC) / (mySC + opSC);
        }
        public int evalMobility()
        {
            int myMoveCount = _acctualBoard.FindAllPossibleMoves(_tmpBoard, _acctualPlayer).Count;
            int opMoveCount = _acctualBoard.FindAllPossibleMoves(_tmpBoard, _oponent).Count;
            return 100 * (myMoveCount - opMoveCount) / (myMoveCount + opMoveCount + 1);
        }
        public int evalCorner()
        {
            int myCorners = 0;
            int opCorners = 0;

            if (_tmpBoard[0, 0] == _acctualPlayer) myCorners++;
            if (_tmpBoard[7, 0] == _acctualPlayer) myCorners++;
            if (_tmpBoard[0, 7] == _acctualPlayer) myCorners++;
            if (_tmpBoard[7, 7] == _acctualPlayer) myCorners++;

            if (_tmpBoard[0, 0] == _oponent) opCorners++;
            if (_tmpBoard[7, 0] == _oponent) opCorners++;
            if (_tmpBoard[0, 7] == _oponent) opCorners++;
            if (_tmpBoard[7, 7] == _oponent) opCorners++;

            return 100 * (myCorners - opCorners) / (myCorners + opCorners + 1);
        }
        public int evalBoardMap()
        {
            int[,] W = {
                {200 , -100, 100,  50,  50, 100, -100,  200},
                {-100, -200, -50, -50, -50, -50, -200, -100},
                {100 ,  -50, 100,   0,   0, 100,  -50,  100},
                {50  ,  -50,   0,   0,   0,   0,  -50,   50},
                {50  ,  -50,   0,   0,   0,   0,  -50,   50},
                {100 ,  -50, 100,   0,   0, 100,  -50,  100},
                {-100, -200, -50, -50, -50, -50, -200, -100},
                {200 , -100, 100,  50,  50, 100, -100,  200}};

            //if corners are taken W for that 1/4 loses effect
            if (_tmpBoard[0, 0] != 0)
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j <= 3; j++)
                    {
                        W[i, j] = 0;
                    }
                }
            }

            if (_tmpBoard[0, 7] != 0)
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 4; j <= 7; j++)
                    {
                        W[i, j] = 0;
                    }
                }
            }

            if (_tmpBoard[7, 0] != 0)
            {
                for (int i = 5; i < 8; i++)
                {
                    for (int j = 0; j <= 3; j++)
                    {
                        W[i, j] = 0;
                    }
                }
            }

            if (_tmpBoard[7, 7] != 0)
            {
                for (int i = 5; i < 8; i++)
                {
                    for (int j = 4; j <= 7; j++)
                    {
                        W[i, j] = 0;
                    }
                }
            }


            int myW = 0;
            int opW = 0;

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (_tmpBoard[i, j] == _acctualPlayer) myW += W[i, j];
                    if (_tmpBoard[i, j] == _oponent) opW += W[i, j];
                }
            }

            return (myW - opW) / (myW + opW + 1);
        }
        public int evalParity()
        {
            int remDiscs = 64 - _acctualBoard.GetAllPawns(_tmpBoard);
            return remDiscs % 2 == 0 ? -1 : 1;
        }

        #endregion

    }
}
