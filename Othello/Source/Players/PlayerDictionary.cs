﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Othello
{
    static class PlayerDictionary
    {
        public static readonly Dictionary<int, Player> PlayerList = new Dictionary<int, Player>
        {
            {0, new PCGreedyPlayer()},
            {1, new PCMaxMin()},
            {2, new PCNegaMax()},
            {3, new PCAlfaBeta()},
            {4, new PCAspiration()},
            {5, new PCMonteCarlo()},
        };


    }
}
