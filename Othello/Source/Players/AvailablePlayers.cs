﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Othello
{
    public enum AvaliblePlayers
    { 
        GreedyPlayer = 0,
        MinMaxPlayer = 1,
        NegaMaxPlayer = 2,
        AlphaBetaPlayer = 3,
        Aspiration = 4,
        PCMonteCarlo = 5
    };
}
