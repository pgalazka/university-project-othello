﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Othello
{
    class PCGreedyPlayer : Player
    {
        public Board _acctualBoard;
        
        public PCGreedyPlayer(Board _board = null)
        {
            _acctualBoard = _board;
        }
        public override List<Vector2> Move()
        {
            List<Vector3> _bestMoves = FindBestMove(_acctualBoard.FindAllPossibleMoves(_acctualBoard._board));
            int randomMove = new Random().Next(0, _bestMoves.Count());
            List <Vector2> slots = new List<Vector2>();
            var _allReversePosition = _acctualBoard.FindAllReversed((int)_bestMoves[randomMove].X, (int)_bestMoves[randomMove].Y);
            slots.AddRange(_allReversePosition);
            slots.Add(new Vector2(_bestMoves[randomMove].X, _bestMoves[randomMove].Y));
            return slots;
        }


        public override void SetBoard(Board board, int whoAmI)
        {
            _acctualBoard = board;
        }
        private List<Vector3> FindBestMove(List<Vector2> _allPossibleMoves) 
        { 
            List<Vector3> _bestMoves = new List<Vector3>();
            List<Vector2> _allReversePosition;
            foreach (var possiblemove in _allPossibleMoves)
            {
                int bestScore = 0;
                if (_bestMoves.Count() > 0)
                    bestScore = (int) _bestMoves[0].Z;
                _allReversePosition = _acctualBoard.FindAllReversed((int)possiblemove.X, (int)possiblemove.Y);
                int acctualScore = _allReversePosition.Count();

                if (acctualScore >= bestScore)
                {
                    if (acctualScore > bestScore)
                    {
                        _bestMoves.Clear();
                        _bestMoves.Add(new Vector3(possiblemove.X, possiblemove.Y, acctualScore));
                    }
                    if (acctualScore == bestScore)
                    {
                        _bestMoves.Add(new Vector3(possiblemove.X, possiblemove.Y, acctualScore));
                    }
                }
            }

            return _bestMoves;
        }
    }
}
