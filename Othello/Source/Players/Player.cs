﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Othello
{
    abstract class Player
    {
        public abstract void SetBoard(Board board, int whoAmI);
        public abstract List<Vector2> Move();
    }
}
