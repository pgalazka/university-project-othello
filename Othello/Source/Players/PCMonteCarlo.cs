﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Othello
{
    class PCMonteCarlo : Player
    {
        private Board _acctualBoard;
        private int _acctualPlayer;
        private int _oponent;
        MonteCarloNode root = new MonteCarloNode();
        public override List<Vector2> Move()
        {
            
            root._board = _acctualBoard._board;
            root.childs.AddRange(GenerateChilds());
            int id = MonteCarlo(root, 10);
            List<Vector2> moves = new List<Vector2>();
            moves.Add(new Vector2(root.childs[id].x, root.childs[id].y));
            moves.AddRange(_acctualBoard.FindAllReversed(root.childs[id].x, root.childs[id].y));
            return moves;
        }

        public override void SetBoard(Board board, int whoAmI)
        {
            _acctualBoard = board;
            _acctualPlayer = whoAmI;
            _oponent = _acctualPlayer == 1 ? 2 : 1;
        }

        public int MonteCarlo(MonteCarloNode node, int nofSimulation)
        {
            MonteCarloNode bestChild = null;
            int bestChildID = -1;
            int bestProbability = -1;
            int index = 0;
            foreach (var child in node.childs)
            {
                int r = 0;
                for (int i = 0; i < nofSimulation; i++)
                {
                    MonteCarloNode _child = child;
                    while (_acctualBoard.isGameOver(_child._board))
                    {
                        
                        LegalMove(_child);
                    }
                    string player = _acctualPlayer == 1 ? "Black" : "White";

                    if (_acctualBoard.getWinner(_child._board) == player)
                        r++;
                }
                    int probability = r / nofSimulation;
                    if(probability > bestProbability)
                    {
                        bestProbability = probability;
                        bestChild = child;
                        bestChildID = index;
                    }
                
                index++;
            }
            return bestChildID;
        }

        void LegalMove(MonteCarloNode node)
        {
            List<Vector2> moves = new List<Vector2>();
            
            List<Vector2> possibleMoves = _acctualBoard.FindAllPossibleMoves(node._board, _acctualPlayer);
            int randomPosition;
            Random rnd = new Random();
            randomPosition = rnd.Next(0, possibleMoves.Count - 1);
            moves.Add(possibleMoves[randomPosition]);
            moves.AddRange(_acctualBoard.FindAllReversed((int)possibleMoves[randomPosition].X, (int)possibleMoves[randomPosition].Y));
            node.x = (int)possibleMoves[randomPosition].X;
            node.y = (int)possibleMoves[randomPosition].Y;
            foreach (var item in moves)
            {
                node._board[(int)item.X, (int)item.Y] = _acctualPlayer;
            }
            moves.Clear();
            possibleMoves = _acctualBoard.FindAllPossibleMoves(node._board, _oponent);
            randomPosition = rnd.Next(0, possibleMoves.Count);
            moves.Add(possibleMoves[randomPosition]);
            moves.AddRange(_acctualBoard.FindAllReversed((int)possibleMoves[randomPosition].X, (int)possibleMoves[randomPosition].Y));
            foreach (var item in moves)
            {
                node._board[(int)item.X, (int)item.Y] = _acctualPlayer;
            }
        }


        List<MonteCarloNode> GenerateChilds()
        {
            List<MonteCarloNode> childs = new List<MonteCarloNode>();
            List<Vector2> moves = new List<Vector2>();
            List<Vector2> possibleMoves = _acctualBoard.FindAllPossibleMoves(_acctualBoard._board, _acctualPlayer);
            for (int i = 0; i < possibleMoves.Count; i++)
            {
                moves.Add(possibleMoves[i]);
                moves.AddRange(_acctualBoard.FindAllReversed((int)possibleMoves[i].X, (int)possibleMoves[i].Y));
                MonteCarloNode newNode = new MonteCarloNode();
                newNode._board = _acctualBoard._board;
                newNode.x = (int)possibleMoves[i].X;
                newNode.y = (int)possibleMoves[i].Y;
                foreach (var item in moves)
                {
                    newNode._board[(int)item.X, (int)item.Y] = _acctualPlayer;
                }
                childs.Add(newNode);
                moves.Clear();
            }

           

            return childs;
        }
    }



    class MonteCarloNode
    {
        public int[,] _board = new int[8, 8];
        public List<MonteCarloNode> childs = new List<MonteCarloNode>();
        public int x = 0;
        public int y = 0;
    }
}
