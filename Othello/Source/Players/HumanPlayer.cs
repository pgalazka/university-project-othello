﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Othello
{
    class HumanPlayer : Player
    {
        private Board _acctualBoard;
        private int _whoAmI;

        public HumanPlayer(Board _board = null)
        {
            _acctualBoard = _board;
        }
        public override void SetBoard(Board board, int whoAmI)
        {
            _whoAmI = whoAmI;
            _acctualBoard = board;
        }
        public override List<Vector2> Move()
        {
            List<Vector2> possibleMoves = _acctualBoard.FindAllPossibleMoves(_acctualBoard._board, _whoAmI);
            List<Vector2> slots = new List<Vector2>();
            int x, y;
            bool correctSlot = false;
            do
            {
                Console.Write("Possible moves: \n--------------------------------\n");
                foreach (var item in possibleMoves)
                {
                    Console.Write(item + " ");
                }
                Console.Write("\n--------------------------------\n Pick board slot: \n>");


                var position = Addons.ReadMultiplyInt(Console.ReadLine());
                x = position[0];
                y = position[1];
                Console.WriteLine(x + " " + y);

                if (possibleMoves.Any(p => p.X == x && p.Y == y))
                    correctSlot = true;
                else
                    Console.WriteLine("Wrong board slot");
            } while (!correctSlot);

            var _allReversePosition = _acctualBoard.FindAllReversed(x, y);
            slots.AddRange(_allReversePosition);
            slots.Add(new Vector2(x, y));
            return slots;
        }
    }
}
